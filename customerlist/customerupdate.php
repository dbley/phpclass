<?php
//DB name, password, etc...
include '../includes/dbConn.php';

//Check if all fields were entered.
if(isset($_POST["txtFirstName"])){
    if(isset($_POST["txtLastName"])){
        if(isset($_POST["txtPhoneNumber"])){
            if(isset($_POST["txtEmail"])){
                if(isset($_POST["txtAddress"])){
                    if(isset($_POST["txtCity"])){
                        if(isset($_POST["numZip"])){
                            if(isset($_POST["selState"])){
                                if(isset($_POST["txtPassword"])){
                                    if(isset($_POST["txtConfirmPassword"])){

                                        //Create variables for each field entry
                                        $id = $_POST["txtID"];
                                        $firstName = $_POST["txtFirstName"];
                                        $lastName = $_POST["txtLastName"];
                                        $phone = $_POST["txtPhoneNumber"];
                                        $email = $_POST["txtEmail"];
                                        $address = $_POST["txtAddress"];
                                        $city = $_POST["txtCity"];
                                        $zip = $_POST["numZip"];
                                        $state = $_POST["selState"];
                                        $pass = $_POST["txtPassword"];


                                        try {
                                            $db = new PDO($dsn, $username, $password, $options);
                                            $sql = $db->prepare("UPDATE customerlist SET firstName = :FirstName, lastName = :LastName, phone = :Phone, email = :Email, address = :Address, city = :City, zip = :Zip, state = :State, password = :Password WHERE customerID = :ID");
                                            $sql->bindValue(":ID", $id);
                                            $sql->bindValue(":FirstName",$firstName);
                                            $sql->bindValue(":LastName",$lastName);
                                            $sql->bindValue(":Phone",$phone);
                                            $sql->bindValue(":Email",$email);
                                            $sql->bindValue(":Address",$address);
                                            $sql->bindValue(":City",$city);
                                            $sql->bindValue(":Zip",$zip);
                                            $sql->bindValue(":State",$state);
                                            $sql->bindValue(":Password",md5($pass . $key));
                                            $sql->execute();

                                        }catch (PDOException $e){
                                            $error = $e->getMessage();
                                            echo "Error: $error";
                                        }

                                        header("Location:customerlist.php");

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
if(isset($_GET["id"])){
    $id=$_GET["id"];
    try {
        $db = new PDO($dsn, $username, $password, $options);
        $sql = $db->prepare("SELECT * FROM customerlist WHERE customerID = :id");
        $sql->bindValue(":id",$id);
        $sql->execute();
        $row = $sql->fetch();
        $firstName = $row["firstName"];
        $lastName = $row["lastName"];
        $phone = $row["phone"];
        $email = $row["email"];
        $address = $row["address"];
        $city = $row["city"];
        $zip = $row["zip"];
        $state = $row["state"];
        $key = $row["keyID"];

    }catch (PDOException $e){
        $error = $e->getMessage();
        echo "Error: $error";
    }

}else{
    header("Location:customerlist.php");
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Derek's Customer Update</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css"/>
    <script type="text/javascript">
        function DeleteCustomer(first, last, id){
            if(confirm("Do you want to delete " + first + " " + last + "?")){

                document.location.href = "customerdelete.php?id=" + id;

            }
        }
    </script>
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <form method="post" >
        <fieldset id="Customer">
            <legend><h3>Customer</h3></legend>
            <label for="txtFirstName"><strong>First Name:</strong></label>
            <input type="text" id="txtFirstName" name="txtFirstName" size="40" value="<?=$firstName?>">
            <br />
            <label for="txtLastName"><strong>Last Name:</strong></label>
            <input type="text" id="txtLastName" name="txtLastName" size="40" value="<?=$lastName?>">
            <br />
            <label for="txtPhoneNumber"><strong>Phone Number:</strong></label>
            <input type="tel" id="txtPhoneNumber" name="txtPhoneNumber" size="15" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" value="<?=$phone?>" maxlength="12">
            <br />
            <label for="txtEmail"><strong>Email:</strong></label>
            <input type="email" id="txtEmail" name="txtEmail" size="40" value="<?=$email?>">
            <br /><br />
        </fieldset>
        <fieldset id="Address">
            <legend><h3>Address</h3></legend>
            <label for="txtAddress"><strong>Address:</strong></label>
            <input type="text" id="txtAddress" name="txtAddress" size="60" value="<?=$address?>">
            <br />
            <label for="txtCity"><strong>City:</strong></label>
            <input type="text" id="txtCity" name="txtCity" size="40" value="<?=$city?>">
            <br />
            <label for="numZip"><strong>Zip Code:</strong></label>
            <input type="number" id="numZip" name="numZip" size="40" maxlength="5" value="<?=$zip?>">
            <br />
            <label for="selState"><strong>State:</strong></label>
            <select id="selState" name="selState" size="1">
                <option value="<?=$state?>" selected><?=$state?></option>
                <option>WI</option>
                <option>TX</option>
                <option>CA</option>
            </select>
            <br /><br />
        </fieldset>
        <fieldset id="Security">
            <legend><h3>Security</h3></legend>
            <label for="txtPassword"><strong>Password:</strong></label>
            <input type="password" id="txtPassword" name="txtPassword" size="40" maxlength="25">
            <br />
            <label for="txtConfirmPassword"><strong>Confirm Password:</strong></label>
            <input type="password" id="txtConfirmPassword" name="txtConfirmPassword" size="40" maxlength="25">
            <br /><br />
            <input type="submit" value="Update Customer"> | <input type="button" onclick="DeleteCustomer('<?=$firstName?>', '<?=$lastName?>', <?=$id?>)" value="Delete Customer">
            <br /><br />
        </fieldset>
        <input type="hidden" id="txtID" name="txtID" value="<?=$id?>">
    </form>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>