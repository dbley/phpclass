<?php

$key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));

//Check if all fields were entered
if(isset($_POST["txtFirstName"])){
    if(isset($_POST["txtLastName"])){
        if(isset($_POST["txtPhoneNumber"])){
            if(isset($_POST["txtEmail"])){
                if(isset($_POST["txtAddress"])){
                    if(isset($_POST["txtCity"])){
                        if(isset($_POST["numZip"])){
                            if(isset($_POST["selState"])){
                                if(isset($_POST["txtPassword"])){
                                    if(isset($_POST["txtConfirmPassword"])){

                                        //Create variables for each field entry
                                        $firstName = $_POST["txtFirstName"];
                                        $lastName = $_POST["txtLastName"];
                                        $phone = $_POST["txtPhoneNumber"];
                                        $email = $_POST["txtEmail"];
                                        $address = $_POST["txtAddress"];
                                        $city = $_POST["txtCity"];
                                        $zip = $_POST["numZip"];
                                        $state = $_POST["selState"];
                                        $pass = $_POST["txtPassword"];
                                        $confirmPass = $_POST["txtConfirmPassword"];

                                        //DB name, password, etc...
                                        include '../includes/dbConn.php';

                                        try {
                                            $db = new PDO($dsn, $username, $password, $options);

                                            $sql = $db->prepare("INSERT INTO customerlist (firstName, lastName, address, city, state, zip, phone, email, password, keyID) VALUE (:FirstName, :LastName, :Address, :City, :State, :Zip, :Phone, :Email, :Password, :Key)");
                                            $sql->bindValue(":FirstName",$firstName);
                                            $sql->bindValue(":LastName",$lastName);
                                            $sql->bindValue(":Address",$address);
                                            $sql->bindValue(":City",$city);
                                            $sql->bindValue(":State",$state);
                                            $sql->bindValue(":Zip",$zip);
                                            $sql->bindValue(":Phone",$phone);
                                            $sql->bindValue(":Email",$email);
                                            $sql->bindValue(":Password",md5($pass . $key));
                                            $sql->bindValue(":Key",$key);
                                            $sql->execute();

                                        }catch (PDOException $e){
                                            $error = $e->getMessage();
                                            echo "Error: $error";
                                        }

                                        header("Location:customerlist.php");

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Derek's Customer Add</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css"/>
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <form method="post" >
        <fieldset id="Customer">
            <legend><h3>Customer</h3></legend>
            <label for="txtFirstName"><strong>First Name:</strong></label>
            <input type="text" id="txtFirstName" name="txtFirstName" size="40">
            <br />
            <label for="txtLastName"><strong>Last Name:</strong></label>
            <input type="text" id="txtLastName" name="txtLastName" size="40">
            <br />
            <label for="txtPhoneNumber"><strong>Phone Number:</strong></label>
            <input type="tel" id="txtPhoneNumber" name="txtPhoneNumber" size="15" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" placeholder="(xxx)xxx-xxxx" maxlength="12">
            <br />
            <label for="txtEmail"><strong>Email:</strong></label>
            <input type="email" id="txtEmail" name="txtEmail" size="40">
            <br /><br />
        </fieldset>
        <fieldset id="Address">
            <legend><h3>Address</h3></legend>
            <label for="txtAddress"><strong>Address:</strong></label>
            <input type="text" id="txtAddress" name="txtAddress" size="60">
            <br />
            <label for="txtCity"><strong>City:</strong></label>
            <input type="text" id="txtCity" name="txtCity" size="40">
            <br />
            <label for="numZip"><strong>Zip Code:</strong></label>
            <input type="number" id="numZip" name="numZip" size="40" maxlength="5" placeholder="5 Digit Zip Code">
            <br />
            <label for="selState"><strong>State:</strong></label>
            <select id="selState" name="selState" size="1">
                <option>WI</option>
                <option>TX</option>
                <option>CA</option>
            </select>
            <br /><br />
        </fieldset>
        <fieldset id="Security">
            <legend><h3>Security</h3></legend>
            <label for="txtPassword"><strong>Password:</strong></label>
            <input type="password" id="txtPassword" name="txtPassword" size="40" maxlength="25">
            <br />
            <label for="txtConfirmPassword"><strong>Confirm Password:</strong></label>
            <input type="password" id="txtConfirmPassword" name="txtConfirmPassword" size="40" maxlength="25" placeholder="re-enter password">
            <br /><br />
            <input type="submit" value="Add Customer">
            <br /><br />
        </fieldset>

    </form>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>