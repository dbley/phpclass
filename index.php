<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Derek's Homepage</title>
    <link rel="stylesheet" type="text/css" href="css/base.css"/>
</head>
<body>
    <header><?php include 'includes/header.php' ?></header>
    <nav><?php include 'includes/nav.php' ?></nav>
    <main>
        <img src="images/derekandleah.jpg" alt="Picture of Derek and his wife, Leah"/>
        <p>He lives with his wife, Leah, and boxer dog, Jordy, in De Pere, WI.  Derek works as wood door purchasing specialist
        for LaForce, Inc. in Green Bay, WI.  Long time passion for sports led to a major interest in fantasy sports, including daily fantasy sports.
        Daily fantasy sports takes a lot of data analysis and led Derek down the route of trying the Software Development program at FVTC.</p>
    </main>
    <footer><?php include 'includes/footer.php' ?></footer>
</body>
</html>
