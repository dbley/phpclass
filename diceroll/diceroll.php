<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Derek's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css"/>
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<div id="dice">
    <?php
    $rolls = array();
    $rolls[0] = mt_rand(1,6);
    $rolls[1] = mt_rand(1,6);
    $rolls[2] = mt_rand(1,6);
    $rolls[3] = mt_rand(1,6);
    $rolls[4] = mt_rand(1,6);
    $rolls[5] = mt_rand(1,6);
    $player = $rolls[0] + $rolls[1] + $rolls[2];
    $computer = $rolls[3] + $rolls[4] + $rolls[5];

    $images = array();
    $images[0] = "../images/dice_$rolls[0].png";
    $images[1] = "../images/dice_$rolls[1].png";
    $images[2] = "../images/dice_$rolls[2].png";
    $images[3] = "../images/dice_$rolls[3].png";
    $images[4] = "../images/dice_$rolls[4].png";
    $images[5] = "../images/dice_$rolls[5].png";

    if($player>$computer){
        $winner = "You Win!";
    }elseif($computer>$player){
        $winner = "Computer Wins";
    }else
        $winner = "Draw!";

    echo "<br />";
    echo "<h3>Your score:$player</h3>";
    echo "<img src=$images[0] /> <img src=$images[1] /> <img src=$images[2] />";
    echo "<h3>Computer's score:$computer</h3>";
    echo "<img src=$images[3] /> <img src=$images[4] /> <img src=$images[5] />";
    echo "<h2>Result:$winner</h2>";

    ?>
</div>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>