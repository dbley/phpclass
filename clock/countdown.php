<?php

/* Countdown timer
End of Summer semester 8/1/2020 */

$secPerMinute = 60;
$secPerHour = 60 * $secPerMinute;
$secPerDay = 24 * $secPerHour;
$secPerYear = 365 * $secPerDay;
//Current year
$now = time();

//End of semester date
$endSemester = mktime(11,0,0,7,31,2020);

//Number of seconds between now and end of semester
$secDiff = $endSemester - $now;

$months = floor($secDiff/$secPerYear * 12);
$secDiff = $secDiff - ($months * ($secPerYear / 12));

$days = floor($secDiff / $secPerDay);
$secDiff = $secDiff - ($days * $secPerDay);

$hours = floor($secDiff / $secPerHour);
$secDiff = $secDiff - ($hours * $secPerHour);

$minutes = floor($secDiff / $secPerMinute);
$secDiff = $secDiff - ($minutes * $secPerMinute);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Derek's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css"/>
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <h3>End of 2020 Summer semester countdown</h3>
    <p><?=$months?> month | <?=$days?> days | <?=$hours?> hours | <?=$minutes?> minutes | <?=$secDiff?> seconds</p>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>
