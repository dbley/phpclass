<?php
session_start();
$errmsg = "";
$key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));

    if($_SESSION["Role"]!=1){
        header("Location:index.php");
    }

    if(isset($_POST["submit"])){
        if(empty($_POST["txtFName"])){
            $errmsg = "Name is required";
        }
        else{
            $FName = $_POST["txtFName"];
        }

        if(empty($_POST["txtEmail"])){
            $errmsg = "Email is required";
        }
        else{
            $Email = $_POST["txtEmail"];
        }

        if(empty($_POST["txtPassword"])){
            $errmsg = "Password is required";
        }
        else{
            $Password = $_POST["txtPassword"];
        }

        if($Password != $_POST["txtPassword2"]){
            $errmsg = "Passwords do not match!!!";
        }

        if(empty($_POST["txtRole"])){
            $errmsg = "Role is required";
        }
        else{
            $Role = $_POST["txtRole"];
        }

        if($errmsg==""){
            //Database work
            //DB name, password, etc...
            include '../includes/dbConn.php';

            try {
                $db = new PDO($dsn, $username, $password, $options);

                $sql = $db->prepare("INSERT INTO memberLogin (memberName, memberEmail, memberPassword, roleID, memberKey) VALUE (:Name, :Email, :Password, :RID, :Key)");
                $sql->bindValue(":Name",$FName);
                $sql->bindValue(":Email",$Email);
                $sql->bindValue(":Password",md5($Password . $key));
                $sql->bindValue(":RID",$Role);
                $sql->bindValue(":Key",$key);
                $sql->execute();

            }catch (PDOException $e){
                $error = $e->getMessage();
                echo "Error: $error";
            }

            $FName = "";
            $Email = "";
            $Password = "";
            $Role = "";

            $errmsg = "Member Added to Database";


        }
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin Page</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css"/>
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <h1>Admin Page</h1>
    <h3 id="error"><?=$errmsg?></h3>
    <form method="post" >
        <table border="1" width="80%">
            <tr height="60">
                <th colspan="2"><h3>Add New Member</h3></th>
            </tr>
            <tr height="40">
                <th>Full Name</th>
                <td><input id="txtFName" name="txtFName" type="text" size="50" required></td>
            </tr>
            <tr height="40">
                <th>Email</th>
                <td><input id="txtEmail" name="txtEmail" type="text" size="50" required></td>
            </tr>
            <tr height="40">
                <th>Password</th>
                <td><input id="txtPassword" name="txtPassword" type="password" size="50" required></td>
            </tr>
            <tr height="40">
                <th>Retype Password</th>
                <td><input id="txtPassword2" name="txtPassword2" type="password" size="50" required></td>
            </tr>
            <tr height="40">
                <th>Role</th>
                <td>
                    <select id="txtRole" name="txtRole" >
                        <?php
                            // DB stuff...
                            include '../../includes/dbConn.php';
                            $db = new PDO($dsn, $username, $password, $options);
                            $sql = $db->prepare("SELECT roleValue FROM role");
                            $sql->execute();
                            $roleValues = $sql->fetchAll();

                            for($i = 0;$i < count($roleValues);$i++){
                                echo "<option value=$i>".$roleValues[$i]."</option>";
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr height="60">
                <td colspan="2">
                    <input type="submit" value="Add New Member" name="submit">
                </td>
            </tr>
        </table>
    </form>
    <br />
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>