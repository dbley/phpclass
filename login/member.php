<?php
session_start();

   if($_SESSION["Role"]!=2 && $_SESSION["Role"]!=3){
        header("Location:index.php");
   }else{
       if($_SESSION["Role"]==2){
           $role = "Promoter";
       }else{
           $role = "Runner";
       }
   }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Member Page</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css"/>
</head>
<body>
<header><?php include '../includes/header.php' ?></header>
<nav><?php include '../includes/nav.php' ?></nav>
<main>
    <h1>Member Page</h1>
        <div id="<?=$role?>">
            <h3><?=$role?></h3>
        </div>
</main>
<footer><?php include '../includes/footer.php' ?></footer>
</body>
</html>