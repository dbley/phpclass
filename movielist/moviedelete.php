<?php


    if(isset($_GET["id"])){
        $id = $_GET["id"];
        try {
            //DB name, password, etc...
            include '../includes/dbConn.php';
            $db = new PDO($dsn, $username, $password, $options);
            $sql = $db->prepare("DELETE FROM movielist WHERE movieID = :id");
            $sql->bindValue(":id",$id);
            $sql->execute();

        }catch (PDOException $e){
            $error = $e->getMessage();
            echo "Error: $error";
        }

    }

    header("Location:movielist.php");

?>
